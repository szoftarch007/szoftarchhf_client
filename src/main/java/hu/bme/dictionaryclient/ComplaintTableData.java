package hu.bme.dictionaryclient;

import javafx.beans.property.SimpleStringProperty;

public class ComplaintTableData {
	private SimpleStringProperty word;
	private SimpleStringProperty description;
	private SimpleStringProperty status;

	public ComplaintTableData(String word, String description, String status) {
		this.word = new SimpleStringProperty(word);
		this.description = new SimpleStringProperty(description);
		this.status = new SimpleStringProperty(status);
	}

	public ComplaintTableData(Complaint complaint) {
		this.word = new SimpleStringProperty(complaint.getWord().getText());
		this.description = new SimpleStringProperty(complaint.getDescription());
		this.status = new SimpleStringProperty(complaint.getSolved() ? "closed" : "open");
	}

	public String getWord() {
		return word.get();
	}

	public void setWord(String word) {
		this.word.set(word);
	}

	public String getDescription() {
		return description.get();
	}

	public void setDescription(String description) {
		this.description.set(description);
		;
	}

	public String getStatus() {
		return status.get();
	}

	public void setStatus(String isOpen) {
		this.status.set(isOpen);
	}

}
