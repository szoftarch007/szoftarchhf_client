package hu.bme.dictionaryclient;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ControllerAdmin {
	@FXML
	private MenuItem menuLogout;
	@FXML
	private MenuItem menuClose;
	@FXML
	private MenuItem menuComplaints;
	@FXML
	private MenuItem menuWords;
	@FXML
	private MenuItem menuTranslation;
	@FXML
	private AnchorPane root;
	
	private MenuActions menuActions = new MenuActions();

	@FXML
	public void menuLogoutAction() {
		menuActions.logoutFromApp(root);
	}

	@FXML
	public void menuCloseAction() {
		menuActions.closeApp(root);
	}

	@FXML
	public void menuComplaintsAction() {
		menuActions.changeMenuAdminComplaints(root);
	}
	
	@FXML
	public void menuTranslationAction() {
		menuActions.changeMenuAdminTranslation(root);
	}

	@FXML
	public void menuWordsAction() {
		menuActions.changeMenuAdminWords(root);
	}

	public void buildAdminWindow(ActionEvent event) {
		try {
			Parent adminParent = FXMLLoader.load(getClass().getResource("AdminWindow.fxml"));
			Scene adminScene = new Scene(adminParent);
			Stage adminStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			adminStage.setScene(adminScene);
			adminStage.setTitle("Dictionary Application - Administrator");
			adminStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
