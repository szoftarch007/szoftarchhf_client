package hu.bme.dictionaryclient;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.print.attribute.standard.Copies;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Response;

public class ControllerAdmin_Complaints implements Initializable {
	@FXML
	private MenuItem menuLogout;
	@FXML
	private MenuItem menuClose;
	@FXML
	private MenuItem menuComplaints;
	@FXML
	private MenuItem menuTranslation;
	@FXML
	private MenuItem menuWords;
	@FXML
	private TableView<ComplaintTableData> tableComplaints;
	@FXML
	private Button btnCloseComplaint;
	@FXML
	AnchorPane root;
	@FXML
	private Text txtOutput;

	private MenuActions menuActions = new MenuActions();
	private List<Complaint> complaints;

	@FXML
	public void menuLogoutAction() {
		menuActions.logoutFromApp(root);
	}

	@FXML
	public void menuCloseAction() {
		menuActions.closeApp(root);
	}

	@FXML
	public void menuComplaintsAction() {

	}

	@FXML
	public void menuWordsAction() {
		menuActions.changeMenuAdminWords(root);
	}

	@FXML
	public void menuTranslationAction() {
		menuActions.changeMenuAdminTranslation(root);
	}

	@FXML
	public void btnCloseComplaintAction() {
		int idx = tableComplaints.getSelectionModel().getSelectedIndex();
		Complaint actualComplaint = complaints.get(idx);
		ServiceComplaint cs = NetworkManager.getComplaintService();
		Call<Void> call = cs.closeComplaint(actualComplaint.getId(), TokenManager.token);
		try {
			Response<Void> response = call.execute();
			int responseCode = response.code();
			if (responseCode == 200) {
				// code 200 --> ok
				txtOutput.setText("Complaint closed!");
				txtOutput.setFill(Color.GREEN);
			} else if (responseCode == 401) {
				// code 401 --> Unauthorized
				txtOutput.setText("Complaint couldn't be closed! Unauthorized user!");
				txtOutput.setFill(Color.RED);
			} else {
				txtOutput.setText("Something went wrong! Response code was: " + responseCode);
				txtOutput.setFill(Color.RED);
			}
			
			refreshComplaints();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void buildAdminWindow_Complaints(AnchorPane _root) {
		try {
			Parent adminParent = FXMLLoader.load(getClass().getResource("AdminWindow_Complaints.fxml"));
			Scene adminScene = new Scene(adminParent);
			Stage adminStage = (Stage) _root.getScene().getWindow();
			adminStage.setScene(adminScene);
			adminStage.setTitle("Dictionary Application - Administrator - Complaiints");
			adminStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void initialize(URL location, ResourceBundle resources) {
		refreshComplaints();
	}

	private void refreshComplaints() {
		ServiceComplaint compServ = NetworkManager.getComplaintService();

		Call<GetComplaintResponse> call = compServ.getComplaint(null, TokenManager.token);
		try {
			Response<GetComplaintResponse> response = call.execute();

			switch (response.code()) {
			case 200:
				ObservableList<ComplaintTableData> complaints = FXCollections.observableArrayList();

				this.complaints = response.body().getComplaints();

				for (Complaint comp : this.complaints) {
					complaints.add(new ComplaintTableData(comp));
				}

				TableColumn<ComplaintTableData, String> wordCol = new TableColumn<ComplaintTableData, String>("Word");
				TableColumn<ComplaintTableData, String> descCol = new TableColumn<ComplaintTableData, String>("Description");
				TableColumn<ComplaintTableData, String> isOpenCol = new TableColumn<ComplaintTableData, String>("status");

				tableComplaints.getColumns().clear();
				tableComplaints.getColumns().addAll(wordCol, descCol, isOpenCol);

				wordCol.setCellValueFactory(new PropertyValueFactory<ComplaintTableData, String>("word"));
				descCol.setCellValueFactory(new PropertyValueFactory<ComplaintTableData, String>("description"));
				isOpenCol.setCellValueFactory(new PropertyValueFactory<ComplaintTableData, String>("status"));
				tableComplaints.setItems(complaints);
				break;
			default:
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Communication error occurred");

				alert.showAndWait();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
