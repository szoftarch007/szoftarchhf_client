package hu.bme.dictionaryclient;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Response;

public class ControllerAdmin_Translation {
	@FXML
	private TextField txtTranslateFrom;
	@FXML
	private TextField txtTranslateTo;
	@FXML
	private TextField txtLinkOfPicture;
	@FXML
	private Button btnTranslation;
	@FXML
	private AnchorPane root;
	@FXML
	private MenuItem menuLogout;
	@FXML
	private MenuItem menuClose;
	@FXML
	private MenuItem menuComplaints;
	@FXML
	private MenuItem menuWords;
	@FXML
	private MenuItem menuTranslation;
	@FXML
	private Text txtOutput;

	private MenuActions menuActions = new MenuActions();

	@FXML
	public void menuLogoutAction() {
		menuActions.logoutFromApp(root);
	}

	@FXML
	public void menuCloseAction() {
		menuActions.closeApp(root);
	}

	@FXML
	public void menuComplaintsAction() {
		menuActions.changeMenuAdminComplaints(root);
	}

	@FXML
	public void menuTranslationAction() {
	}

	@FXML
	public void menuWordsAction() {
		menuActions.changeMenuAdminWords(root);
	}
	
	@FXML
	public void btnTranslateAction(){
		ServiceWord ws = NetworkManager.getWordService();
		TranslationRequestData trd = new TranslationRequestData(txtTranslateFrom.getText(), txtLinkOfPicture.getText(), txtTranslateTo.getText());
		Call<Void> call = ws.createTranslation(TokenManager.token, trd);
		try {
			Response<Void> response = call.execute();
			int responseCode = response.code();
			if (responseCode == 200) {
				txtOutput.setText("Translation created!");
				txtOutput.setFill(Color.GREEN);
			} else {
				txtOutput.setText("Something went wrong! Response code was: " + responseCode);
				txtOutput.setFill(Color.RED);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void buildAdminWindow_Translation(AnchorPane _root) {
		try {
			Parent adminParent = FXMLLoader.load(getClass().getResource("AdminWindow_Translation.fxml"));
			Scene adminScene = new Scene(adminParent);
			Stage adminStage = (Stage) _root.getScene().getWindow();
			adminStage.setScene(adminScene);
			adminStage.setTitle("Dictionary Application - Administrator - Translation");
			adminStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
