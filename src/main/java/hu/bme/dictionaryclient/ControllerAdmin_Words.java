package hu.bme.dictionaryclient;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Response;

public class ControllerAdmin_Words implements Initializable {
	@FXML
	private MenuItem menuLogout;
	@FXML
	private MenuItem menuClose;
	@FXML
	private MenuItem menuComplaints;
	@FXML
	private MenuItem menuWords;
	@FXML
	private MenuItem menuTranslation;
	@FXML
	private TextField txtWord;
	@FXML
	private TextField txtLinkOfPicture;
	@FXML
	private TextField txtDescription;
	@FXML
	private Button btnCreate;
	@FXML
	private AnchorPane root;
	@FXML
	private ComboBox<String> cboxLanguage;

	private MenuActions menuActions = new MenuActions();

	private ServiceLanguage serviceLanguage;

	private ServiceWord serviceWord;

	private ObservableList<String> languages;

	private List<Language> languageObjects;

	@FXML
	public void menuLogoutAction() {
		menuActions.logoutFromApp(root);
	}

	@FXML
	public void menuCloseAction() {
		menuActions.closeApp(root);
	}

	@FXML
	public void menuTranslationAction() {
		menuActions.changeMenuAdminTranslation(root);
	}

	@FXML
	public void menuComplaintsAction() {
		menuActions.changeMenuAdminComplaints(root);
	}

	@FXML
	public void menuWordsAction() {
	}

	@FXML
	public void btnCreateAction() {
		String wordStr = txtWord.getText();
		Language language = languageObjects.get(cboxLanguage.getSelectionModel().getSelectedIndex());
		String link = txtLinkOfPicture.getText();
		String desc = txtDescription.getText();

		Word word = new Word();
		word.setText(wordStr);
		word.setLanguage(language);
		word.setLink(link);
		word.setDescription(desc);

		Call<Void> call = serviceWord.createWord(TokenManager.token, word);
		Response<Void> response;
		try {
			response = call.execute();
			switch (response.code()) {
			case 200:
				onWordCreated(word);
				break;

			case 400:
				onWordCreationError("Bad request");
				break;
			}
		} catch (IOException e) {
			onWordCreationError("No connection");
		}

	}

	private void onWordCreated(Word word) {
		String message = "Word was created: " + word.getText();
		System.out.println(message);
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Confirmation");
		alert.setHeaderText(null);
		alert.setContentText(message);

		alert.showAndWait();
	}

	private void onWordCreationError(String message) {
		System.out.println(message);
	}
	
	public void initialize(URL location, ResourceBundle resources) {
		serviceLanguage = NetworkManager.getLanguageService();

		serviceWord = NetworkManager.getWordService();

		try {
			languages = getLanguages(serviceLanguage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (languages != null)
			cboxLanguage.setItems(languages);
	}

	public void buildAdminWindow_Words(AnchorPane _root) {
		try {

			Parent adminParent = FXMLLoader.load(getClass().getResource("AdminWindow_Words.fxml"));
			Scene adminScene = new Scene(adminParent);
			Stage adminStage = (Stage) _root.getScene().getWindow();
			adminStage.setScene(adminScene);
			adminStage.setResizable(false);
			adminStage.setTitle("Dictionary Application - Administrator - Word Creation");
			adminStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ObservableList<String> getLanguages(ServiceLanguage service) throws IOException {
		Call<AvailableLanguagesResponse> call = service.getAllLanguages();
		Response<AvailableLanguagesResponse> response = call.execute();

		if (response.code() == 200) {
			languageObjects = response.body().getLanguages();
			
			ObservableList<String> languages = FXCollections.observableArrayList();

			for (Language language : languageObjects) {
				languages.add(language.getName());
			}

			return languages;
		} else {
			return null;
		}

	}
}
