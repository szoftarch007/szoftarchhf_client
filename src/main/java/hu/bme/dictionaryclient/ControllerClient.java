package hu.bme.dictionaryclient;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Response;

public class ControllerClient implements Initializable {
	@FXML
	private MenuItem menuLogout;
	@FXML
	private MenuItem menuClose;
	@FXML
	private Text txtUserInfo;
	@FXML
	private TextField txtWordInput;
	@FXML
	private ComboBox<String> cboxLanguageTo;
	@FXML
	private Button btnSearch;
	@FXML
	private Button btnComplain;
	@FXML
	private TextField txtComplaint;
	@FXML
	private AnchorPane root;
	@FXML
	private TableView<WordTableData> tableWords;

	private MenuActions menuActions = new MenuActions();
	private List<Language> languageObjects;
	private List<Word> words;
	private ServiceLanguage serviceLanguage;
	private ObservableList<String> languages;

	@FXML
	public void menuLogoutAction() {
		menuActions.logoutFromApp(root);
	}

	@FXML
	public void menuCloseAction() {
		menuActions.closeApp(root);
	}

	@FXML
	public void btnSearchAction() {
		String langId = languageObjects.get(cboxLanguageTo.getSelectionModel().getSelectedIndex()).getId();
		refreshWordSearch(txtWordInput.getText(), langId);

	}

	private void refreshWordSearch(String term, String lang) {

		ServiceWord ws = NetworkManager.getWordService();

		Call<WordSearchResponse> call = ws.searchWord(term, lang);

		try {
			Response<WordSearchResponse> response = call.execute();

			switch (response.code()) {
			case 200:
				ObservableList<WordTableData> words = FXCollections.observableArrayList();

				this.words = response.body().getFoundWords();

				for (Word w : this.words) {
					words.add(new WordTableData(w));
				}

				TableColumn<WordTableData, String> wordCol = new TableColumn<WordTableData, String>("Word");
				TableColumn<WordTableData, String> descCol = new TableColumn<WordTableData, String>("Description");
				TableColumn<WordTableData, String> linkCol = new TableColumn<WordTableData, String>("Link");
				TableColumn<WordTableData, String> languageCol = new TableColumn<WordTableData, String>("Language");

				tableWords.getColumns().clear();
				tableWords.getColumns().addAll(wordCol, descCol, linkCol, languageCol);

				wordCol.setCellValueFactory(new PropertyValueFactory<WordTableData, String>("text"));
				descCol.setCellValueFactory(new PropertyValueFactory<WordTableData, String>("description"));
				linkCol.setCellValueFactory(new PropertyValueFactory<WordTableData, String>("link"));
				languageCol.setCellValueFactory(new PropertyValueFactory<WordTableData, String>("language"));
				tableWords.setItems(words);
				break;
			default:
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Communication error occurred");

				alert.showAndWait();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	public void btnComplainAction() {
		int idx = tableWords.getSelectionModel().getSelectedIndex();
		if (idx >= 0) {
			Word actualWord = words.get(idx);

			ServiceComplaint cs = NetworkManager.getComplaintService();
			Call<Void> call = cs.createComplaint(new ComplaintData(txtComplaint.getText(), actualWord.getText()),
					TokenManager.token);
			try {
				Response<Void> response = call.execute();
				int responseCode = response.code();
				if (responseCode == 200) {
					// code 200 --> ok
					txtUserInfo.setText("Complaint created!");
					txtUserInfo.setFill(Color.GREEN);
				} else {
					txtUserInfo.setText("Something went wrong! Response code was: " + responseCode);
					txtUserInfo.setFill(Color.RED);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void buildClientWindow(ActionEvent event) {
		try {
			Parent clientParent = FXMLLoader.load(getClass().getResource("ClientWindow.fxml"));
			Scene clientScene = new Scene(clientParent);
			Stage clientStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
			clientStage.setScene(clientScene);
			clientStage.setTitle("Dictionary Application - Administrator");
			clientStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ObservableList<String> getLanguages(ServiceLanguage service) throws IOException {
		Call<AvailableLanguagesResponse> call = service.getAllLanguages();
		Response<AvailableLanguagesResponse> response = call.execute();

		if (response.code() == 200) {
			languageObjects = response.body().getLanguages();

			ObservableList<String> languages = FXCollections.observableArrayList();

			for (Language language : languageObjects) {
				languages.add(language.getName());
			}

			return languages;
		} else {
			return null;
		}

	}

	public void initialize(URL location, ResourceBundle resources) {
		serviceLanguage = NetworkManager.getLanguageService();
		try {
			languages = getLanguages(serviceLanguage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (languages != null)
			cboxLanguageTo.setItems(languages);

	}
}
