package hu.bme.dictionaryclient;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Response;

public class ControllerLogin {
	@FXML
	private TextField txtUsername;
	@FXML
	private PasswordField txtPassword;
	@FXML
	private Button btnLogin;
	@FXML
	private Button btnRegister;
	@FXML
	private Text txtOutput;

	@FXML
	void btnLoginAction(ActionEvent event) {
		ServiceLogin ls = NetworkManager.getLoginService();
		Call<LoginResponse> call = ls.login(new Credentials(txtUsername.getText(), txtPassword.getText()));
		try {
			Response<LoginResponse> response = call.execute();
			int responseCode = response.code();
			if (responseCode == 200) {
				// code 200 --> ok
				TokenManager.token = response.body().getSessionToken();
				if (response.body().getRole().getName().equals("admin")) {
					ControllerAdmin windowAdmin = new ControllerAdmin();
					windowAdmin.buildAdminWindow(event);
				} else {
					ControllerClient windowClient = new ControllerClient();
					windowClient.buildClientWindow(event);
				}

			} else if (responseCode == 404) {
				// code 404 --> no such username found
				txtOutput.setText("User not found: " + txtUsername.getText() + "!");
				txtOutput.setFill(Color.RED);

			} else if (responseCode == 401) {
				// code 401 --> wrong password
				txtOutput.setText("Incorrect password!");
				txtOutput.setFill(Color.RED);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@FXML
	void btnRegisterAction(ActionEvent event) {
		ServiceRegistration rs = NetworkManager.getRegistrationService();
		Call<Void> call = rs.register(new Credentials(txtUsername.getText(), txtPassword.getText()));
		try {
			Response<Void> response = call.execute();
			int responseCode = response.code();
			if (responseCode == 201) {
				// code 201 --> created
				txtOutput.setText("Registration was successful for user: " + txtUsername.getText());
				txtOutput.setFill(Color.GREEN);
			} else if (responseCode == 409) {
				// code 409 --> conflict, already exists
				txtOutput.setText("Registered user: " + txtUsername.getText() + " already exists!");
				txtOutput.setFill(Color.RED);
			} else {
				txtOutput.setText("Something went wrong! Response code was: " + responseCode);
				txtOutput.setFill(Color.RED);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void buildLoginWindow(Stage primaryStage) {
		try {
			Parent loginParent = FXMLLoader.load(getClass().getResource("LoginWindow.fxml"));
			Scene loginScene = new Scene(loginParent);
			primaryStage.setScene(loginScene);
			primaryStage.setTitle("Dictionary Application - Login");
			primaryStage.show();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setRegisterClickListener(EventHandler<ActionEvent> e) {
		btnRegister.setOnAction(e);
	}

	public String getUserName() {
		return txtUsername.getText();
	}

	public String getPassword() {
		return txtPassword.getText();
	}
}
