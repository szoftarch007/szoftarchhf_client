/*
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hu.bme.dictionaryclient;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.google.gson.annotations.SerializedName;

/**
 * GetComplaintResponse
 */
public class GetComplaintResponse {
	@SerializedName("complaints")
	private List<Complaint> complaints = new ArrayList<Complaint>();

	public GetComplaintResponse complaints(List<Complaint> complaints) {
		this.complaints = complaints;
		return this;
	}

	public GetComplaintResponse addComplaintsItem(Complaint complaintsItem) {
		this.complaints.add(complaintsItem);
		return this;
	}

	/**
	 * Get complaints
	 * 
	 * @return complaints
	 **/
	public List<Complaint> getComplaints() {
		return complaints;
	}

	public void setComplaints(List<Complaint> complaints) {
		this.complaints = complaints;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		GetComplaintResponse getComplaintResponse = (GetComplaintResponse) o;
		return Objects.equals(this.complaints, getComplaintResponse.complaints);
	}

	@Override
	public int hashCode() {
		return Objects.hash(complaints);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class GetComplaintResponse {\n");

		sb.append("    complaints: ").append(toIndentedString(complaints)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
