package hu.bme.dictionaryclient;

import java.io.IOException;

import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import retrofit2.Call;
import retrofit2.Response;

public class MenuActions {

	public void logoutFromApp(AnchorPane root) {
		ServiceLogout sl = NetworkManager.getLogoutService();
		Call<Void> call = sl.logout();
		Response<Void> response;
		try {
			response = call.execute();
			int responseCode = response.code();
			if (responseCode > 0) {
				// code 200 --> ok
				TokenManager.token = null;
				ControllerLogin loginWindow = new ControllerLogin();
				loginWindow.buildLoginWindow((Stage) root.getScene().getWindow());
			} else {
				// popup that sg went wrong
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void closeApp(AnchorPane root) {
		Stage primaryStage = (Stage) root.getScene().getWindow();
		primaryStage.close();
	}

	public void changeMenuAdminComplaints(AnchorPane root) {
		ControllerAdmin_Complaints windowComplaints = new ControllerAdmin_Complaints();
		windowComplaints.buildAdminWindow_Complaints(root);
	}

	public void changeMenuAdminWords(AnchorPane root) {
		ControllerAdmin_Words windowWords = new ControllerAdmin_Words();
		windowWords.buildAdminWindow_Words(root);
	}
	
	public void changeMenuAdminTranslation(AnchorPane root) {
		ControllerAdmin_Translation windowTranslation = new ControllerAdmin_Translation();
		windowTranslation.buildAdminWindow_Translation(root);
	}

}
