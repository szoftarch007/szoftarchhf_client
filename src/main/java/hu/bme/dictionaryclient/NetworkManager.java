package hu.bme.dictionaryclient;

import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkManager {
	public static final String baseUrl = "http://localhost:8080/dictionary/"; 
	private static ServiceRegistration registrationService;
	private static ServiceLogin loginService;
	private static ServiceLogout logoutService;
	private static ServiceLanguage languageService;
	private static ServiceWord wordService;
        private static ServiceComplaint complaintService;
	
	public static ServiceRegistration getRegistrationService(){
		if(registrationService == null){
			registrationService = getRetrofit().create(ServiceRegistration.class);
		}
		return registrationService;
	}
	
	public static ServiceLogin getLoginService(){
		if(loginService == null){
			loginService = getRetrofit().create(ServiceLogin.class);
		}
		return loginService;
	}
	
	public static ServiceLogout getLogoutService(){
		if(logoutService == null){
			logoutService = getRetrofit().create(ServiceLogout.class);
		}
		return logoutService;
	}

	public static ServiceLanguage getLanguageService(){
		if(languageService == null){
			languageService = getRetrofit().create(ServiceLanguage.class);
		}
		return languageService;
	}
	
	public static ServiceWord getWordService(){
		if(wordService == null){
			wordService = getRetrofit().create(ServiceWord.class);
		}
		return wordService;
	}

	public static ServiceComplaint getComplaintService(){
		if(complaintService == null){
			complaintService = getRetrofit().create(ServiceComplaint.class);
		}
		return complaintService;
	}
	
	private static OkHttpClient getHttpClient() {
		return new OkHttpClient();
	}
	
	private static Retrofit getRetrofit() {
		return new Retrofit.Builder().baseUrl(baseUrl).client(getHttpClient())
				.addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create())).build();
	}
}
