package hu.bme.dictionaryclient;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceComplaint {
	@POST("complaint/create")
	Call<Void> createComplaint(@Body ComplaintData complaintData, @Header("X-Authorization") String token);

	@POST("complaint/close")
	Call<Void> closeComplaint(@Query("id") Long id, @Header("X-Authorization") String token);

	@GET("complaint")
	Call<GetComplaintResponse> getComplaint(@Query("id") Long id, @Header("X-Authorization") String token);
}
