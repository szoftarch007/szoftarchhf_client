package hu.bme.dictionaryclient;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServiceLanguage {
	@GET("language/all")
	Call<AvailableLanguagesResponse> getAllLanguages(); 
	
	@GET("language/get")
	Call<LanguageResponse> getLanguage(@Query("code") String code);
}
