package hu.bme.dictionaryclient;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServiceLogin {
	@POST("login")
	Call<LoginResponse> login(@Body Credentials credentials);

}