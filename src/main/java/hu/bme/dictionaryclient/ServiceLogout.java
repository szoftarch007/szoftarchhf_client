package hu.bme.dictionaryclient;

import retrofit2.Call;
import retrofit2.http.POST;

public interface ServiceLogout {
	@POST("logout")
	Call<Void> logout();
}
