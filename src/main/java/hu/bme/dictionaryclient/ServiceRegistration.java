package hu.bme.dictionaryclient;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ServiceRegistration {
	@POST("register")
	Call<Void> register(@Body Credentials credentials);

}
