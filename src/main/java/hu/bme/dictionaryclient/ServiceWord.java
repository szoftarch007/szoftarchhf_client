package hu.bme.dictionaryclient;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceWord {

	@POST("word/create")
	Call<Void> createWord(@Header("X-Authorization") String token, @Body Word word);

	@GET("word/search")
	Call<WordSearchResponse> searchWord(@Query("term") String term, @Query("lang") String lang);

	@GET("word/translate")
	Call<WordTranslationResponse> translateWord(@Query("word") String word, @Query("lang") String lang);

	@POST("word/translation/create")
	Call<Void> createTranslation(@Header("X-Authorization") String token,
			@Body TranslationRequestData translationRequestData);

}
