package hu.bme.dictionaryclient;

import javafx.beans.property.SimpleStringProperty;

public class WordTableData {
	
	private SimpleStringProperty text;
	private SimpleStringProperty description;
	private SimpleStringProperty link;
	private SimpleStringProperty language;
	public WordTableData(Word word) {
		this.text = new SimpleStringProperty(word.getText());
		this.description = new SimpleStringProperty(word.getDescription());
		this.link = new SimpleStringProperty(word.getLink());
		this.language = new SimpleStringProperty(word.getLanguage().getName());
	}
	public String getText() {
		return text.get();
	}
	public void setText(String text) {
		this.text.set(text);
	}
	public String getDescription() {
		return description.get();
	}
	public void setDescription(String description) {
		this.description.set(description);
	}
	public String getLink() {
		return link.get();
	}
	public void setLink(String link) {
		this.link.set(link);
	}
	public String getLanguage() {
		return language.get();
	}
	public void setLanguage(String language) {
		this.language.set(language);
	}
	
	
	

}
